// libs block
const readline = require('readline-sync');
const fs = require('fs');
const path = require('path');

const readFile=(filePath) => {
    if(fs.existsSync(filePath)){
        return fs.readFileSync(filePath);
    }
    return null;
}

// const writeFile=(filePath) => {
//     if(fs.existsSync(filePath)){
//         return fs.writeFileSync(filePath);
//     }
//     return null;
// }

const dirPath = path.resolve(__dirname, 'data');
const filePath = path.resolve(dirPath, 'employees.json');

const file = readFile(filePath);
const content = file && JSON.parse(file) || [];
var employees = content;

// 
class Employee{
    constructor(name,surname,gender,age,department,position,salary){
        this.name=name;
        this.surname=surname;
        this.gender=gender;
        this.age=age;
        this.department=department;
        this.position=position;
        this.salary=salary;
    }
    info(){
        console.log('name: ',this.name,'\nsurname: ',this.surname,
        '\ngender: ',this.gender,'\nage: ',this.age,'\ndepartment: ',this.department,
        '\nposition: ',this.position,'\nsalary: ',this.salary);
    }
}

const getEmployeeNumber=()=>{
    let num=0;
    while(num<1||num>employees.length)
        num=readline.question("Employee's num: ");
        // num=Number(answer);
        // employees[num-1].info();
    return num-1;
}

const actNumber=()=>{
    let act=-1;
    let q='1. Add;\
    \n2. Info;\
    \n3. Edit;\
    \n4. Delete;\
    \n5. Info All;\
    \n0. Exit.\
    \n>> ';
    let num;
    while(act!=0){    
        act=Number(readline.question(q));
        console.log('You choose '+act);
    
        switch(act){
            case 1:{
                let name = readline.question("Employee's name: ");
                let surname = readline.question("Employee's surname: ");
                let gender = readline.question("Employee's gender: ");
                let age = readline.question("Employee's age: ");
                let department = readline.question("Employee's department: ");
                let position = readline.question("Employee's position: ");
                let salary = Number(readline.question("Employee's salary: "));

                let newEmployee = new Employee(name, surname, gender, age, department, position, salary);
                employees.push(newEmployee);

                break;
            }
            case 2:{
                num = getEmployeeNumber();
                console.log(employees[num]);
                break;
            }
            case 3:{
                num = getEmployeeNumber();
                employees[num].name = readline.question("Employee's name: ");
                employees[num].surname = readline.question("Employee's surname: ");
                employees[num].gender = readline.question("Employee's gender: ");
                employees[num].age = readline.question("Employee's age: ");
                employees[num].department = readline.question("Employee's department: ");
                employees[num].position = readline.question("Employee's position: ");
                employees[num].salary = Number(readline.question("Employee's salary: "));
                break;
            }
            case 4:{
                num = getEmployeeNumber();
                delete employees[num];
                employees.splice(num,1);
                break;
            }
            case 5:{
                for(let i=0;i<employees.length;i++){
                    console.log('#',(i+1),')');
                    console.log(employees[i]);
                }
            }
            default:{
                console.log('Exit done');
            }
        }
    }
}

// console.log(employees);
if(employees.length===0){
    let vasya=new Employee('Vasily','Petrov','male',20,'GUI','jr.',11000);
    let petya=new Employee('Petr','Ivanov','male',26,'GUI','md.',24000);
    let katya=new Employee('Ekaterina','Kuznetsova','female',22,'GUI','md.',22000);
    employees.push(vasya);
    employees.push(petya);
    employees.push(katya);
    for(let i=0;i<employees.length;i++){
        console.log('#',(i+1),')');
        employees[i].info();
    }
}
else{
    for(let i=0;i<employees.length;i++){
        console.log('#',(i+1),')');
        console.log(employees[i]);
    }
}

actNumber();

const jsonContent = JSON.stringify(employees, null, 2);
fs.mkdirSync(dirPath, {recursive:true});
fs.writeFileSync(filePath, jsonContent);

